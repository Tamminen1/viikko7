/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viikko7;

/**
 *
 * @author Juha
 */
public abstract class Account {
    String tili1 = "";
    String tili2 ="";
    int raha1 = 0;
    int raha2 =0;
    int luotto = 0;
    void talletus(String x, int y) {
        if(x.equals(tili1)==true) {
            raha1 = raha1 + y;
        }
        else if(x.equals(tili2)==true) {
            raha2 = raha2 + y;
        }
    }
    void nosto(String x, int y) {
        if(x.equals(tili1)==true && (raha1 - y)>=0) {
            raha1 = raha1 - y;
        }
        else if(x.equals(tili2)==true && (raha2 - y)>=-luotto) {
            raha2 = raha2 - y;
        }
    }
    void poista(String x) {
        if(x.equals(tili1)==true) {
            tili1 = "";
            raha1 = 0;
        }
        else if(x.equals(tili2)==true) {
            tili2 = "";
            raha2 = 0;
            luotto = 0;
        }
    }
    void tulosta(String x) {
        if(x.equals(tili1)==true) {
            System.out.println("Tilinumero: " + tili1 + " Tilillä rahaa: " + raha1);
        }
        else if(x.equals(tili2)==true) {
            System.out.println("Tilinumero: " + tili2 + " Tilillä rahaa: " + raha2);
        }
    }
    void tulostakaikki() {
        if(tili1.equals("")!=true) {
            System.out.println("Tilinumero: " + tili1 + " Tilillä rahaa: " + raha1);
        }
        if(tili2.equals("")!=true) {
            System.out.println("Tilinumero: " + tili2 + " Tilillä rahaa: " + raha2 + " Luottoraja: " + luotto);
        }
    }
}
class tili extends Account {
    public tili() {
    }
    public void tili1(String x, int y) {
        tili1 = x;
        raha1 = y;
    }
    public void tili2(String x, int y, int z) {
        tili2 = x;
        raha2 = y;
        luotto = z;
    }
}