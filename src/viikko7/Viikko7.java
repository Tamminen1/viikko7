/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viikko7;

import java.util.Scanner;

/** Date 19.10.2016
 * opiskelijanumero 0437786
 * @author Juha Tamminen
 */
public class Viikko7 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String temp;
        String tili;
        int raha;
        int luotto;
        Bank d1 = new Bank();
        while(true) {
            System.out.print("\n*** PANKKIJÄRJESTELMÄ ***\n" + "1) Lisää tavallinen tili\n" + "2) Lisää luotollinen tili\n" + "3) Tallenna tilille rahaa\n" + "4) Nosta tililtä\n" + "5) Poista tili\n" + "6) Tulosta tili\n" + "7) Tulosta kaikki tilit\n" + "0) Lopeta\n" + "Valintasi: ");
            Scanner scan = new Scanner(System.in);
            temp = scan.nextLine();
            if(temp.equals("1")==true) {
                System.out.print("Syötä tilinumero: ");
                tili = scan.nextLine();
                System.out.print("Syötä rahamäärä: ");
                raha = scan.nextInt();
                d1.lisääTili(tili, raha);
            }
            else if(temp.equals("2")==true) {
                System.out.print("Syötä tilinumero: ");
                tili = scan.nextLine();
                System.out.print("Syötä rahamäärä: ");
                raha = scan.nextInt();
                System.out.print("Syötä luottoraja: ");
                luotto = scan.nextInt();
                d1.lisääLuotollinenTili(tili, raha, luotto);
            }
            else if(temp.equals("3")==true) {
                System.out.print("Syötä tilinumero: ");
                tili = scan.nextLine();
                System.out.print("Syötä rahamäärä: ");
                raha = scan.nextInt();
                d1.talletus(tili, raha);
            }
            else if(temp.equals("4")==true) {
                System.out.print("Syötä tilinumero: ");
                tili = scan.nextLine();
                System.out.print("Syötä rahamäärä: ");
                raha = scan.nextInt();
                d1.nosto(tili, raha);
            }
            else if(temp.equals("5")==true) {
                System.out.print("Syötä poistettava tilinumero: ");
                tili = scan.nextLine();
                d1.poistaTili(tili);
            }
            else if(temp.equals("6")==true) {
                System.out.print("Syötä tulostettava tilinumero: ");
                tili = scan.nextLine();
                d1.etsiTili(tili);
            }
            else if(temp.equals("7")==true) {
                d1.tulostaTilit();
            }
            else if(temp.equals("0")==true) {
                break;
            }
            else {
                System.out.println(temp);
                System.out.println("Valinta ei kelpaa.");
            }
        }       
    }    
}
