/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viikko7;

/**
 *
 * @author Juha
 */
public class Bank {
    tili d1 = new tili();
    public void lisääTili(String x, int y) {
        d1.tili1(x, y);
        System.out.println("Tili luotu.");
    }
    public void lisääLuotollinenTili(String x, int y, int z) {
        d1.tili2(x, y, z);
        System.out.println("Tili luotu.");
    }
    public void talletus(String x, int y) {
        d1.talletus(x, y);
    }
    public void nosto(String x, int y) {
        d1.nosto(x, y);
    }
    public void poistaTili(String x) {
        d1.poista(x);
        System.out.println("Tili poistettu.");
    }
    public void tulostaTilit() {
        System.out.println("Kaikki tilit:");
        d1.tulostakaikki();
    }
    public void etsiTili(String x) {
        d1.tulosta(x);
    }
}
